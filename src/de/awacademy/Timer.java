package de.awacademy;

import de.awacademy.model.Model;
import javafx.animation.AnimationTimer;
import javafx.geometry.Rectangle2D;
import javafx.scene.shape.Rectangle;

import java.util.ConcurrentModificationException;

public class Timer extends AnimationTimer {

    private Graphics graphics;
    private Model model;

    public Timer(Graphics graphics, Model model) {
        this.graphics = graphics;
        this.model = model;
    }

    @Override
    public void handle(long nowNano) {

        try {
            model.update(nowNano);
            graphics.draw();
        } catch (Exception e) {
        }


    }
}
