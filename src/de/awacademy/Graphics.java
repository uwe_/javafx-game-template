package de.awacademy;

import de.awacademy.model.Aliens;
import de.awacademy.model.Bullets;
import de.awacademy.model.Model;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Graphics {
    Image background;
    Image alexWflames;
    Image alexWOflames;
    Image jonasAstro;
    Image playerBullet;
    Image explosion;
    Image alienBullet;

    private GraphicsContext gc;
    private Model model;

    public Graphics(GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
        try {
            background = new Image(new FileInputStream("src/de/awacademy/images/starbackground.png"));
            alexWflames = new Image(new FileInputStream("src/de/awacademy/images/AlexWflames2.png"));
            alexWOflames = new Image(new FileInputStream("src/de/awacademy/images/AlexWOflames2.png"));
            jonasAstro = new Image(new FileInputStream("src/de/awacademy/images/JonasAstro.png"));
            playerBullet = new Image(new FileInputStream("src/de/awacademy/images/bulletPlayer.png"));
            alienBullet = new Image(new FileInputStream("src/de/awacademy/images/bulletAliens.png"));
            explosion = new Image(new FileInputStream("src/de/awacademy/images/explosion.png"));
        } catch (
                FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public void draw() throws FileNotFoundException {
        //Game Over
        if (model.isStop()) {
            gc.setFill(Color.BLACK);
            gc.fillRect(0, 0, 600, 600);
            gc.setFill(Color.GHOSTWHITE);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.setFont(Font.font("Calibri", 35));
            String go = "Game Over!\n Your Score: " + model.getPlayer().getScore() + "\nPress Enter to restart";
            gc.fillText(go, 300, 300);
            return;
        }

        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);

        //Background

        gc.drawImage(background, 0, 0);

        //Player
        if (model.isFlames()) {

            gc.drawImage(alexWflames,
                    model.getPlayer().getX(), model.getPlayer().getY(),
                    model.getPlayer().getW(), model.getPlayer().getH());
        } else {
            gc.drawImage(alexWOflames,
                    model.getPlayer().getX(), model.getPlayer().getY(),
                    model.getPlayer().getW(), model.getPlayer().getH());
        }

        //Aliens
        model.getAlienList().forEach(alien -> gc.drawImage(jonasAstro,
                alien.getX(), alien.getY(), alien.getW(), alien.getH()));

        //Bullets
        if (model.getBullet() != null) {
            model.getBulletList().forEach(Bullets -> gc.drawImage(playerBullet, Bullets.getX(), Bullets.getY()));
        }
        model.getBulletListAliens().forEach(Bullets -> gc.drawImage(alienBullet, Bullets.getX(), Bullets.getY()));

        //Score
        gc.drawImage(explosion, -50, 550);

        gc.setFill(Color.BLACK);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setFont(Font.font("Calibri", 14));
        String s = "Score: " + model.getPlayer().getScore();
        gc.fillText(s, 24, 595);
    }
}


