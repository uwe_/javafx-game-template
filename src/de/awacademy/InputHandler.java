package de.awacademy;

import de.awacademy.model.Bullets;
import de.awacademy.model.Model;
import javafx.animation.AnimationTimer;
import javafx.scene.input.KeyCode;

public class InputHandler {

    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keycode) {
        if (model.getPlayer().getX() <= 490) {
            if (keycode == KeyCode.RIGHT) {
                model.getPlayer().move(20, 0);
                model.setFlames(true);
            }
        }
        if (model.getPlayer().getX() >= 10) {
            if (keycode == KeyCode.LEFT) {
                model.getPlayer().move(-20, 0);
                model.setFlames(true);
            }
        }
        if (keycode == KeyCode.SPACE) {
            model.createBullet();
            model.addBullet(model.getBullet());
        }
        if (keycode == KeyCode.ENTER) {  //Reset
            model.setStop();
        }

    }
}
