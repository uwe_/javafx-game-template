package de.awacademy.model;

public class Bullets {
    private int x;
    private int y;
    private int h;
    private int w;

    public Bullets(int x, int y) {  //x,y sind die Startwerte
        this.x = x;
        this.y = y;
        this.h = 20;
        this.w = 10;
    }

    public void shoot(){
        this.y -= 9;
    }

    public void shootAliens(){
        this.y += 6;

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

}
