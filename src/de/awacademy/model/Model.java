package de.awacademy.model;

import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.Random;

public class Model {

    private Player player;
    private Bullets bullet;
    private Aliens alien;
    private int counter;
    private boolean stop = false;
    private boolean flames = false;

    LinkedList<Bullets> bulletList = new LinkedList<>();
    LinkedList<Bullets> bulletListAliens = new LinkedList<>();
    LinkedList<Aliens> alienList = new LinkedList<>();

    public final double WIDTH = 600;
    public final double HEIGHT = 600;

    public Model() {
        this.player = new Player(230, 500);
        createAlienList(5);
    }

    public Player getPlayer() {
        return player;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //Listen Getter
    public LinkedList<Bullets> getBulletList() {
        return bulletList;
    }

    public LinkedList<Bullets> getBulletListAliens() {
        return bulletListAliens;
    }

    public LinkedList<Aliens> getAlienList() {
        return alienList;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //Bullets
    public Bullets getBullet() {
        return bullet;
    }

    public void createBullet() {
        Bullets bullet = new Bullets(getPlayer().getX() + 50, getPlayer().getY() - 35); //Player Position, da die Kugeln von hier aus starten
        this.bullet = bullet;
    }

    public void addBullet(Bullets bullet) { //Bullets des Players
        bulletList.add(bullet);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //Aliens
    public Aliens getAlien() {
        return alien;
    }

    public Aliens createAlien(int X_A) {
        Aliens alien = new Aliens(X_A, getAlien().getY());
        this.alien = alien;
        return this.alien;
    }

    public void addAlien(Aliens alien) {
        alienList.add(alien);
    }

    public void createAlienList(int anzahlAliens) {
        for (int j = 0; j < anzahlAliens; j++) {
            this.alien = new Aliens(10, 10);
            if (!alienList.isEmpty()) {
                addAlien(createAlien(alien.getX() + 114 + alienList.get(j - 1).getX()));
            } else {
                addAlien(createAlien(alien.getX()));
            }
        }
    }

    public void addAlienBullets() {
        for (Aliens alien : alienList) {
            if (new Random().nextBoolean()) {   //Random!
                bulletListAliens.add(new Bullets(alien.getX() + 35, alien.getY() + 80));
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //Update and Miscellaneous

    public boolean isFlames() {
        return flames;
    }

    public void setFlames(boolean flames) {
        this.flames = flames;
    }

    public boolean isStop() {
        return stop;
    }

    public boolean setStop() {
        bulletList = new LinkedList<>();
        bulletListAliens = new LinkedList<>();
        alienList = new LinkedList<>();
        stop = false;
        player.setScore();
        return stop;
    }

    //Collision Conditions
    public boolean cond1(int bulletPos, int bulletSize, int alienPos) {
        boolean cond = bulletPos + bulletSize >= alienPos;
        return cond;
    }

    public boolean cond2(int bulletPos, int alienPos, int alienSize) {
        boolean cond = bulletPos <= alienPos + alienSize;
        return cond;
    }

    public void update(long nowNano) {  //UPDATE

        counter++;
        if (counter == 85) {
            addAlienBullets();
            counter = 0;
        }

        for (Bullets alienBullets : bulletListAliens) {
            alienBullets.shootAliens();
        }

        if (this.bullet != null) {
            for (Bullets playerBullets : bulletList) {
                playerBullets.shoot();
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //Collision-Conditions

        for (Aliens alien : alienList) {
            for (Bullets playerBullets : bulletList) {
                boolean cond1 = cond1(playerBullets.getX(), playerBullets.getW(), alien.getX());
                boolean cond2 = cond2(playerBullets.getX(), alien.getX(), alien.getW());

                if (cond1 && cond2 && playerBullets.getY() <= alien.getH()) {
                    player.incScore();
                    try {
                        alienList.remove(alien);
                        bulletList.remove(playerBullets);
                    } catch (ConcurrentModificationException e) {
                    }
                } else if (playerBullets.getY() <= 0) {
                    try {
                        bulletList.remove(playerBullets);
                    } catch (ConcurrentModificationException e) {
                    }
                }
            }
        }
        if (alienList.isEmpty()) {
            createAlienList(5);
        }
        for (Bullets alienBullets : bulletListAliens) {
            boolean cond1 = cond1(alienBullets.getX(), alienBullets.getW(), player.getX());
            boolean cond2 = cond2(alienBullets.getX(), player.getX(), player.getW());

            if (cond1 && cond2 && alienBullets.getY() >= player.getY()) {
                stop = true;
            } else if (alienBullets.getY() >= 570) {
                try {
                    bulletListAliens.remove(alienBullets);
                } catch (ConcurrentModificationException e) {
                }
            }
        }


    }
}
