package de.awacademy.model;

public class Player {

    private int x;
    private int y;
    private int h;
    private int w;
    private int score;

    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 100;
        this.w = 120;
        this.score = 0;
    }

    public void move(int dx, int dy) {
        this.x += dx;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public int getScore(){
        return score;
    }
    public void setScore(){
        score = 0;
    }
    public void incScore(){
        this.score += 1;
    }

}
