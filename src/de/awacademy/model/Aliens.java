package de.awacademy.model;

public class Aliens {
    private int x;
    private int y;
    private int h;
    private int w;

    public Aliens(int x, int y) {  //x, y sind die Startwerte
        this.x = x;
        this.y = y;
        this.h = 80;
        this.w = 80;
    }

    public void move(int dx, int dy){  //Vielleicht später um die Aliens nach unten zu bewegen
        this.x += dx;
        this.y += dy;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

}
